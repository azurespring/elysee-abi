<?php
/*
 * EAddress.php
 */

namespace AzureSpring\Elysee\ABI\Types;

/**
 * address: equivalent to uint160, except for the assumed interpretation and language typing.
 * For computing the function selector, address is used.
 */
class EAddress extends EUInt
{
}
