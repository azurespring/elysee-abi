<?php
/*
 * EBytes.php
 */

namespace AzureSpring\Elysee\ABI\Types;

/**
 * bytes<M>: binary type of M bytes, 0 < M <= 32.
 * bytes: dynamic sized byte sequence.
 */
class EBytes implements TypeInterface
{
    private $x;

    private $dynamic;

    /**
     * Constructor.
     *
     * @param string $x
     * @param bool   $dynamic
     */
    public function __construct(string $x, $dynamic = false)
    {
        $this->x = $x;
        $this->dynamic = $dynamic;
    }

    /**
     * @return string
     */
    public function pluck()
    {
        return $this->x;
    }

    /**
     * @inheritDoc
     */
    public function isDynamic(): bool
    {
        return $this->dynamic;
    }

    /**
     * @inheritDoc
     */
    public function encode(): string
    {
        $n = strlen($this->x);
        $s = '';
        if ($this->dynamic) {
            $s = (new EUInt($n))->encode();
        }

        return $s.$this->x.str_repeat("\0", (($n + 31) & ~31) - $n);
    }
}
