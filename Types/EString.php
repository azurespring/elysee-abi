<?php
/*
 * EString.php
 */

namespace AzureSpring\Elysee\ABI\Types;

/**
 * string: dynamic sized unicode string assumed to be UTF-8 encoded.
 */
class EString extends EBytes
{
    /**
     * Constructor.
     *
     * @param string $x
     */
    public function __construct(string $x)
    {
        parent::__construct($x, true);
    }
}
