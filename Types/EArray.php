<?php
/*
 * EArray.php
 */

namespace AzureSpring\Elysee\ABI\Types;

/**
 * <type>[M]: a fixed-length array of M elements, M >= 0, of the given type.
 * <type>[]: a variable-length array of elements of the given type.
 */
class EArray extends ETuple
{
    private $dynamic;

    /**
     * Constructor.
     *
     * @param TypeInterface[] $xs
     * @param bool            $dynamic
     */
    public function __construct(array $xs, bool $dynamic = false)
    {
        parent::__construct($xs);

        $this->dynamic = $dynamic;
    }

    /**
     * @return bool
     */
    public function isDynamic(): bool
    {
        return $this->dynamic || parent::isDynamic();
    }

    /**
     * @inheritDoc
     */
    public function encode(): string
    {
        $s = '';
        if ($this->dynamic) {
            $s = (new EUInt(count($this->xs)))->encode();
        }

        return $s.parent::encode();
    }
}
