<?php
/*
 * AbstractStatic.php
 */

namespace AzureSpring\Elysee\ABI\Types;

/**
 * Static types
 */
abstract class AbstractStatic implements TypeInterface
{
    /**
     * @inheritDoc
     */
    public function isDynamic(): bool
    {
        return false;
    }
}
