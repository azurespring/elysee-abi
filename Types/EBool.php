<?php
/*
 * EBool.php
 */

namespace AzureSpring\Elysee\ABI\Types;

/**
 * bool: equivalent to uint8 restricted to the values 0 and 1.
 * For computing the function selector, bool is used.
 */
class EBool extends EUInt
{
    /**
     * Constructor.
     *
     * @param bool $x
     */
    public function __construct(bool $x)
    {
        parent::__construct(gmp_init($x ? 1 : 0));
    }

    /**
     * @return bool
     */
    public function pluck()
    {
        return gmp_testbit($this->x, 0);
    }
}
