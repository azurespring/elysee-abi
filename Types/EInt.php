<?php
/*
 * EInt.php
 */

namespace AzureSpring\Elysee\ABI\Types;

/**
 * int<M>: two’s complement signed integer type of M bits, 0 < M <= 256, M % 8 == 0.
 */
class EInt extends AbstractStatic
{
    private $x;

    /**
     * Constructor.
     *
     * @param int|string|\GMP $x
     */
    public function __construct($x)
    {
        $this->x = $x instanceof \GMP ? $x : gmp_init($x);
    }

    /**
     * @return \GMP
     */
    public function pluck()
    {
        return $this->x;
    }

    /**
     * @inheritDoc
     */
    public function encode(): string
    {
        $x = $this->x;
        if (gmp_sign($x) < 0) {
            $x += gmp_init(1) << 256;
        }

        $s = gmp_strval($x, 16);

        return hex2bin(str_repeat('0', 64 - strlen($s)).$s);
    }
}
