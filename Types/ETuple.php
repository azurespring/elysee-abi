<?php
/*
 * ETuple.php
 */

namespace AzureSpring\Elysee\ABI\Types;

/**
 * (T1,T2,...,Tn): tuple consisting of the types T1, …, Tn, n >= 0
 */
class ETuple implements TypeInterface
{
    protected $xs;

    /**
     * Constructor.
     *
     * @param TypeInterface[] $xs
     */
    public function __construct(array $xs)
    {
        $this->xs = $xs;
    }

    /**
     * @return array
     */
    public function pluck()
    {
        return $this->xs;
    }

    /**
     * @inheritDoc
     */
    public function isDynamic(): bool
    {
        foreach ($this->xs as $x) {
            if ($x->isDynamic()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function encode(): string
    {
        $n = array_sum(array_map(
            function (TypeInterface $x) {
                return $x->isDynamic() ? 32 : strlen($x->encode());
            },
            $this->xs
        ));

        $heads = $tails = '';
        foreach ($this->xs as $x) {
            if (!$x->isDynamic()) {
                $heads .= $x->encode();
            } else {
                $heads .= (new EUInt($n + strlen($tails)))->encode();
                $tails .= $x->encode();
            }
        }

        return $heads.$tails;
    }
}
