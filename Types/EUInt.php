<?php
/*
 * EUInt.php
 */

namespace AzureSpring\Elysee\ABI\Types;

/**
 * uint<M>: unsigned integer type of M bits, 0 < M <= 256, M % 8 == 0. e.g. uint32, uint8, uint256.
 */
class EUInt extends AbstractStatic
{
    protected $x;

    /**
     * Constructor.
     *
     * @param int|string|\GMP $x
     */
    public function __construct($x)
    {
        $this->x = $x instanceof \GMP ? $x : gmp_init($x);
    }

    /**
     * @return \GMP
     */
    public function pluck()
    {
        return $this->x;
    }

    /**
     * @inheritDoc
     */
    public function encode(): string
    {
        $s = gmp_strval($this->x, 16);

        return hex2bin(str_repeat('0', 64 - strlen($s)).$s);
    }
}
