<?php
/*
 * EType.php
 */

namespace AzureSpring\Elysee\ABI\Types;

/**
 * ABI Type
 */
interface TypeInterface
{
    /**
     * @return mixed
     */
    public function pluck();

    /**
     * @return bool
     */
    public function isDynamic(): bool;

    /**
     * @return string
     */
    public function encode(): string;
}
