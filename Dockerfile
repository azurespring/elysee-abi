FROM php:7.2

RUN apt-get update && apt-get install -y \
	libgmp-dev \
	unzip \
	zlib1g-dev
RUN docker-php-ext-install \
	gmp \
	zip
