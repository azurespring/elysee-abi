<?php
/*
 * TupleDecoder.php
 */

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\ETuple;

/**
 * TupleDecoder
 */
class TupleDecoder implements DecoderInterface
{
    private $decoders;

    /**
     * Constructor.
     *
     * @param DecoderInterface[] $decoders
     */
    public function __construct(array $decoders)
    {
        $this->decoders = $decoders;
    }

    /**
     * @inheritDoc
     */
    public function isDynamic(): bool
    {
        foreach ($this->decoders as $decoder) {
            if ($decoder->isDynamic()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function decode($s)
    {
        $begin = ftell($s);
        $headDecoder = new UIntDecoder();

        return new ETuple(array_map(
            function (DecoderInterface $decoder, $x) use ($s, $begin) {
                if (!$x instanceof \GMP) {
                    return $x;
                }

                fseek($s, $begin + gmp_intval($x));

                return $decoder->decode($s);
            },
            $this->decoders,
            array_map(
                function (DecoderInterface $decoder) use ($s, $headDecoder) {
                    return $decoder->isDynamic() ? $headDecoder->decode($s)->pluck() : $decoder->decode($s);
                },
                $this->decoders
            )
        ));
    }
}
