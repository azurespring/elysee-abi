<?php
/*
 * IntDecoder.php
 */

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\EInt;

/**
 * IntDecoder
 */
class IntDecoder extends AbstractStaticDecoder
{
    /**
     * @param resource $s
     *
     * @return EInt
     */
    public function decode($s)
    {
        $n = gmp_init(bin2hex(fread($s, 32)), 16);
        if (gmp_testbit($n, 255)) {
            $n -= gmp_init(1) << 256;
        }

        return new EInt($n);
    }
}
