<?php
/*
 * BytesDecoder.php
 */

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\EBytes;

/**
 * BytesDecoder
 */
class BytesDecoder implements DecoderInterface
{
    private $m;

    /**
     * Constructor.
     *
     * @param int|null $m
     */
    public function __construct(?int $m = null)
    {
        $this->m = $m;
    }

    /**
     * @inheritDoc
     */
    public function isDynamic(): bool
    {
        return !$this->m;
    }

    /**
     * @param resource $s
     *
     * @return EBytes
     */
    public function decode($s)
    {
        if ($this->m) {
            return new EBytes(substr(fread($s, 32), 0, $this->m));
        }

        $decoder = new UIntDecoder();
        $n = gmp_intval($decoder->decode($s)->pluck());

        return new EBytes(substr(fread($s, ($n + 31) & ~31), 0, $n), true);
    }
}
