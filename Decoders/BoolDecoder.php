<?php
/*
 * BoolDecoder.php
 */

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\EBool;

/**
 * BoolDecoder
 */
class BoolDecoder extends AbstractStaticDecoder
{
    /**
     * @param resource $s
     *
     * @return EBool
     */
    public function decode($s)
    {
        return new EBool(fread($s, 32) !== str_repeat("\0", 32));
    }
}
