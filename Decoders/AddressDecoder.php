<?php
/*
 * AddressDecoder.php
 */

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\EAddress;

/**
 * AddressDecoder
 */
class AddressDecoder extends AbstractStaticDecoder
{
    /**
     * @param resource $s
     *
     * @return EAddress
     */
    public function decode($s)
    {
        return new EAddress(gmp_init(bin2hex(fread($s, 32)), 16));
    }
}
