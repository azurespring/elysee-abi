<?php
/*
 * UIntDecoder.php
 */

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\EUInt;

/**
 * UIntDecoder
 */
class UIntDecoder extends AbstractStaticDecoder
{
    /**
     * @param resource $s
     *
     * @return EUInt
     */
    public function decode($s)
    {
        return new EUInt(gmp_init(bin2hex(fread($s, 32)), 16));
    }
}
