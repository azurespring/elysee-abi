<?php
/*
 * AbstractStaticDecoder.php
 */

namespace AzureSpring\Elysee\ABI\Decoders;

/**
 * AbstractStaticDecoder
 */
abstract class AbstractStaticDecoder implements DecoderInterface
{
    /**
     * @inheritDoc
     */
    public function isDynamic(): bool
    {
        return false;
    }
}
