<?php
/*
 * DecoderInterface.php
 */

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\TypeInterface;

/**
 * DecoderInterface
 */
interface DecoderInterface
{
    /**
     * @return bool
     */
    public function isDynamic(): bool;

    /**
     * @param resource $s
     *
     * @return TypeInterface
     */
    public function decode($s);
}
