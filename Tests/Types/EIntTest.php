<?php

namespace AzureSpring\Elysee\ABI\Types;

use PHPUnit\Framework\TestCase;

class EIntTest extends TestCase
{
    /**
     * @dataProvider encodingProvider
     */
    public function testEncode($expected, $x)
    {
        $x = new EInt($x);
        $this->assertEquals(hex2bin($expected), $x->encode());
    }

    public function encodingProvider()
    {
        return [
            [ '0000000000000000000000000000000000000000000000000000000000000002', 2 ],
            [ '0000000000000000000000000000000000000000000000000123456789ABCDEF', '0x0123456789ABCDEF' ],
            [ 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD', -3 ],
            [ '8000000000000000000000000000000000000000000000000000000000000000', '-0x8000000000000000000000000000000000000000000000000000000000000000' ],
        ];
    }
}
