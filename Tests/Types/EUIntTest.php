<?php

namespace AzureSpring\Elysee\ABI\Types;

use PHPUnit\Framework\TestCase;

class EUIntTest extends TestCase
{
    /**
     * @dataProvider constructionProvider
     */
    public function testConstruct($x)
    {
        $this->assertInstanceOf(EUInt::class, new EUInt($x));
    }

    public function constructionProvider()
    {
        return [
            [ 2 ],
            [ '3' ],
            [ gmp_init(5) ],
        ];
    }

    /**
     * @dataProvider pluckingProvider
     */
    public function testPluck($expected, $x)
    {
        $x = new EUInt($x);
        $this->assertEquals($expected, $x->pluck());
    }

    public function pluckingProvider()
    {
        return [
            [ gmp_init(2), 2 ],
            [ gmp_init('3'), '3' ],
            [ gmp_init(5), gmp_init(5) ],
        ];
    }

    /**
     * @dataProvider encodingProvider
     */
    public function testEncode($expected, $x)
    {
        $x = new EUInt($x);
        $this->assertEquals(hex2bin($expected), $x->encode());
    }

    public function encodingProvider()
    {
        return [
            [ '0000000000000000000000000000000000000000000000000000000000000002', 2 ],
            [ '0000000000000000000000000000000000000000000000000123456789ABCDEF', '0x0123456789ABCDEF' ],
        ];
    }
}
