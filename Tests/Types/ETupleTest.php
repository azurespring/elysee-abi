<?php

namespace AzureSpring\Elysee\ABI\Types;

use PHPUnit\Framework\TestCase;

class ETupleTest extends TestCase
{
    /**
     * @dataProvider isDynamicProvider
     */
    public function testIsDynamic($expected, $xs)
    {
        $x = new ETuple($xs);
        $this->assertEquals($expected, $x->isDynamic());
    }

    public function isDynamicProvider()
    {
        return [
            [false, [new EUInt(2), new EBytes('hello')]],
            [true, [new EInt(3), new EBytes('world', true)]],
        ];
    }

    /**
     * @dataProvider encodingProvider
     */
    public function testEncode($expected, $xs)
    {
        $x = new ETuple($xs);
        $this->assertEquals($expected, $x->encode());
    }

    public function encodingProvider()
    {
        return [
            [
                "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x02hello\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0",
                [new EUInt(2), new EBytes('hello')],
            ],
            [
                "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x03\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x40\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x05world\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0",
                [new EInt(3), new EBytes('world', true)],
            ]
        ];
    }
}
