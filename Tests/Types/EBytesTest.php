<?php

namespace AzureSpring\Elysee\ABI\Types;

use PHPUnit\Framework\TestCase;

class EBytesTest extends TestCase
{
    /**
     * @dataProvider encodingProvider
     */
    public function testEncode($expected, $x, $dynamic)
    {
        $x = new EBytes($x, $dynamic);
        $this->assertEquals($expected, $x->encode());
    }

    public function encodingProvider()
    {
        return [
            ["hello\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 'hello', false],
            ["\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x05hello\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 'hello', true],
        ];
    }
}
