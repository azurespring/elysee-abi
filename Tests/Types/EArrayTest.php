<?php

namespace AzureSpring\Elysee\ABI\Types;

use PHPUnit\Framework\TestCase;

class EArrayTest extends TestCase
{
    /**
     * @dataProvider isDynamicProvider
     */
    public function testIsDynamic($expected, $xs, $dynamic)
    {
        $x = new EArray($xs, $dynamic);
        $this->assertEquals($expected, $x->isDynamic());
    }

    public function isDynamicProvider()
    {
        return [
            [false, [new EUInt(2), new EUInt(3)], false],
            [true, [new EUInt(2), new EUInt(3)], true],
            [true, [new EString('Ada'), new EString('Bob')], false],
        ];
    }

    /**
     * @dataProvider encodingProvider
     */
    public function testEncode($expected, $xs, $dynamic)
    {
        $x = new EArray($xs, $dynamic);
        $this->assertEquals($expected, $x->encode());
    }

    public function encodingProvider()
    {
        return [
            [
                "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x02\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x03",
                [new EUInt(2), new EUInt(3)],
                false,
            ],
            [
                "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x02\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x02\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x03",
                [new EUInt(2), new EUInt(3)],
                true,
            ],
            [
                "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x40\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x80\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x03Ada\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x03Bob\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0",
                [new EString('Ada'), new EString('Bob')],
                false,
            ],
        ];
    }
}
