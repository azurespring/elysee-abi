<?php

namespace AzureSpring\Elysee\ABI\Types;

use PHPUnit\Framework\TestCase;

class EBoolTest extends TestCase
{
    public function testConstruct()
    {
        $this->assertInstanceOf(EBool::class, new EBool(false));
        $this->assertInstanceOf(EBool::class, new EBool(true));
    }

    public function testPluck()
    {
        $this->assertFalse((new EBool(false))->pluck());
        $this->assertTrue((new EBool(true))->pluck());
    }
}
