<?php

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\EBytes;
use AzureSpring\Elysee\ABI\Types\EInt;
use AzureSpring\Elysee\ABI\Types\ETuple;
use AzureSpring\Elysee\ABI\Types\EUInt;
use PHPUnit\Framework\TestCase;

class TupleDecoderTest extends TestCase
{
    /**
     * @dataProvider isDynamicProvider
     */
    public function testIsDynamic($expected, $decoders)
    {
        $this->assertEquals($expected, (new TupleDecoder($decoders))->isDynamic());
    }

    public function isDynamicProvider()
    {
        return [
            [false, [new UIntDecoder(), new BytesDecoder(2)]],
            [true, [new UIntDecoder(), new BytesDecoder()]],
        ];
    }

    /**
     * @dataProvider decodingProvider
     */
    public function testDecode($xs, $decoders, $s)
    {
        $s = fopen('data://plain/text;base64,'.base64_encode($s), 'r');
        $decoder = new TupleDecoder($decoders);
        $this->assertEquals(new ETuple($xs), $decoder->decode($s));
        fclose($s);
    }

    public function decodingProvider()
    {
        return [
            [
                [new EUInt(2), new EBytes('hello')],
                [new UIntDecoder(), new BytesDecoder(5)],
                "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x02hello\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0",
            ],
            [
                [new EInt(3), new EBytes('world', true)],
                [new IntDecoder(), new BytesDecoder()],
                "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x03\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x40\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x05world\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0",
            ],
        ];
    }
}
