<?php

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\EBytes;
use PHPUnit\Framework\TestCase;

class BytesDecoderTest extends TestCase
{
    /**
     * @dataProvider decodingProvider
     */
    public function testDecode($expected, $m, $s)
    {
        $decoder = new BytesDecoder($m);
        $s = fopen('data://text/plain;base64,'.base64_encode($s), 'r');
        $this->assertEquals($expected, $decoder->decode($s));
        fclose($s);
    }

    public function decodingProvider()
    {
        return [
            [new EBytes('hello'), 5, "hello\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"],
            [new EBytes('world', true), null, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x05world\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"],
        ];
    }
}
