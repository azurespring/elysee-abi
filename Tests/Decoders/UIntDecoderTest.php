<?php

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\EUInt;
use PHPUnit\Framework\TestCase;

class UIntDecoderTest extends TestCase
{
    /**
     * @dataProvider decodingProvider
     */
    public function testDecode($expected, $s)
    {
        $decoder = new UIntDecoder();
        $s = fopen('data://text/plain;base64,'.base64_encode($s), 'r');
        $this->assertEquals($expected, $decoder->decode($s));
        fclose($s);
    }

    public function decodingProvider()
    {
        return [
            [new EUInt(2), "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x02"],
            [new EUInt(3), "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x03Ada\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"],
        ];
    }
}
