<?php

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\EInt;
use PHPUnit\Framework\TestCase;

class IntDecoderTest extends TestCase
{
    /**
     * @dataProvider decodingProvider
     */
    public function testDecode($expected, $s)
    {
        $decoder = new IntDecoder();
        $s = fopen('data://text/plain;base64,'.base64_encode($s), 'r');
        $this->assertEquals($expected, $decoder->decode($s));
        fclose($s);
    }

    public function decodingProvider()
    {
        return [
            [new EInt(2), "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x02"],
            [new EInt(3), "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\x03Ada\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"],
            [new EInt(-5), "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xfb"],
        ];
    }
}
