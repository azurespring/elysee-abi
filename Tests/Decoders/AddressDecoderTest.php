<?php

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\EAddress;
use PHPUnit\Framework\TestCase;

class AddressDecoderTest extends TestCase
{
    public function testDecode()
    {
        $decoder = new AddressDecoder();
        $s = fopen("data://text/plain;base64,".base64_encode("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x5a\xd9\x06\xe5\x2e\x03\x31\x31\xc6\x52\x52\x51\xa9\x85\x73\x24\xb9\x10\x4c\x08"), 'r');
        $this->assertEquals(new EAddress(gmp_init('0x5Ad906e52e033131c6525251A9857324b9104c08')), $decoder->decode($s));
        fclose($s);
    }
}
