<?php

namespace AzureSpring\Elysee\ABI\Decoders;

use AzureSpring\Elysee\ABI\Types\EBool;
use PHPUnit\Framework\TestCase;

class BoolDecoderTest extends TestCase
{
    /**
     * @dataProvider decodingProvider
     */
    public function testDecode($expected, $s)
    {
        $decoder = new BoolDecoder();
        $s = fopen("data://text/plain;base64,".base64_encode($s), 'r');
        $this->assertEquals($expected, $decoder->decode($s));
        fclose($s);
    }

    public function decodingProvider()
    {
        return [
            [new EBool(false), ""],
            [new EBool(true), ""],
        ];
    }
}
